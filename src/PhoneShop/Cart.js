import React from "react";

export default function Cart({ cart, handleTangGiamSoLuong, handleXoaSp }) {
  console.log(cart);
  return (
    <div className="my-5">
      <table className="table">
        <thead className="thead-dark">
          <tr>
            <th>Mã SP</th>
            <th>Hình ảnh</th>
            <th>Tên</th>
            <th>Số lượng</th>
            <th>Đơn giá</th>
            <th>Thành tiền</th>
            <th>Thao tác</th>
          </tr>
        </thead>
        <tbody>
          {cart.map((item) => {
            return (
              <tr>
                <td>{item.maSP}</td>
                <td>
                  <img src={item.hinhAnh} alt="" style={{ width: "60px" }} />
                </td>
                <td>{item.tenSP}</td>
                <td>
                  <button
                    className="btn btn-info"
                    style={{
                      width: "35px",
                      height: "35px",
                      textAlign: "center",
                    }}
                    onClick={() => {
                      handleTangGiamSoLuong(1, item.maSP);
                    }}
                  >
                    +
                  </button>
                  <span className="mx-3"> {item.quantity}</span>
                  <button
                    className="btn btn-primary"
                    style={{
                      width: "35px",
                      height: "35px",
                      textAlign: "center",
                    }}
                    onClick={() => {
                      handleTangGiamSoLuong(-1, item.maSP);
                    }}
                  >
                    -
                  </button>
                </td>
                <td>{item.giaBan}</td>
                <td>{item.giaBan * item.quantity}</td>
                <td>
                  <button
                    onClick={() => {
                      handleXoaSp(item.maSP);
                    }}
                    className="btn btn-danger"
                  >
                    Xóa
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
