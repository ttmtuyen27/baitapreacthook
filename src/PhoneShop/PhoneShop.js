import React, { useCallback, useState } from "react";
import Cart from "./Cart";
import { dataPhoneShop } from "./dataPhoneShop";
import ModalItem from "./ModalItem";
import ProductList from "./ProductList";

export default function PhoneShop() {
  const data = dataPhoneShop;
  const [id, setId] = useState(null);
  const [cart, setCart] = useState([]);
  const changeIdItem = useCallback(
    (idSp) => {
      return setId(idSp);
    },
    [id]
  );
  const handleThemSp = useCallback(
    (id) => {
      let indexCart = cart.findIndex((item) => {
        return item.maSP == id;
      });
      if (indexCart == -1) {
        let index = data.findIndex((item) => {
          return item.maSP == id;
        });
        let item = { ...data[index] };
        item.quantity = 1;
        let cloneCart = [...cart];
        cloneCart.push(item);
        return setCart(cloneCart);
      } else {
        let cloneCart = [...cart];
        cloneCart[indexCart].quantity += 1;
        return setCart(cloneCart);
      }
    },
    [cart]
  );

  const handleTangGiamSoLuong = useCallback(
    (giaTri, id) => {
      let index = cart.findIndex((item) => {
        return item.maSP == id;
      });
      let cloneCart = [...cart];
      cloneCart[index].quantity += giaTri;
      if (cloneCart[index].quantity == 0) {
        cloneCart.splice(index, 1);
      }
      return setCart(cloneCart);
    },
    [cart]
  );

  const handleXoaSp = useCallback(
    (id) => {
      let index = cart.findIndex((item) => {
        return item.maSP == id;
      });
      let cloneCart = [...cart];
      cloneCart.splice(index, 1);
      return setCart(cloneCart);
    },
    [cart]
  );

  return (
    <div className="container my-5">
      <ProductList
        data={data}
        changeIdItem={changeIdItem}
        handleThemSp={handleThemSp}
      />
      <ModalItem id={id} data={data} />
      <h3 className="mt-5">Số lượng sản phẩm trong giỏ hàng: {cart.length}</h3>
      {cart.length > 0 && (
        <Cart
          handleTangGiamSoLuong={handleTangGiamSoLuong}
          handleXoaSp={handleXoaSp}
          cart={cart}
        />
      )}
    </div>
  );
}
