import React from "react";
import Item from "./Item";

export default function ProductList({ data, changeIdItem, handleThemSp }) {
  return (
    <div className="row">
      {data.map((item) => {
        return (
          <Item
            item={item}
            changeIdItem={changeIdItem}
            handleThemSp={handleThemSp}
          />
        );
      })}
    </div>
  );
}
