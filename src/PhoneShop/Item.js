import React from "react";

export default function Item({ item, changeIdItem, handleThemSp }) {
  return (
    <div className="col-4">
      <div
        className="card mx-2"
        style={{ height: "380px", width: "350px", paddingTop: "20px" }}
      >
        <img
          src={item.hinhAnh}
          className="card-img-top"
          style={{ width: "200px", margin: "auto" }}
          alt="..."
        />
        <div className="card-body">
          <h5 className="card-title">{item.tenSP}</h5>
          <p className="card-text">{item.giaBan} vnd</p>
          <a
            className="btn btn-danger text-white"
            style={{ cursor: "pointer" }}
            data-toggle="modal"
            data-target="#productModal"
            onClick={() => {
              changeIdItem(item.maSP);
            }}
          >
            Xem chi tiết
          </a>
          <a
            className="btn btn-success text-white m-2"
            style={{ cursor: "pointer" }}
            onClick={() => {
              handleThemSp(item.maSP);
            }}
          >
            Thêm vào giỏ hàng
          </a>
        </div>
      </div>
    </div>
  );
}
