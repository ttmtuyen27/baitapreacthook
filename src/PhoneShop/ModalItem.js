import React from "react";

export default function ModalItem({ id, data }) {
  let index = data.findIndex((item) => {
    return item.maSP == id;
  });
  if (index == -1) return;
  else {
    return (
      <div>
        <div>
          {/* Modal */}
          <div
            className="modal fade"
            id="productModal"
            tabIndex={-1}
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered modal-lg">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">
                    {data[index].tenSP}
                  </h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <div className="row">
                    <div className="col-4 ">
                      <img
                        src={data[index].hinhAnh}
                        style={{ width: "270px" }}
                        alt=""
                      />
                    </div>
                    <div className="col-8">
                      <h4 className="text-left text-danger pb-2">
                        Thông số kỹ thuật
                      </h4>
                      <table className="table">
                        <tbody>
                          <tr>
                            <td>Màn hình</td>
                            <td>{data[index].manHinh}</td>
                          </tr>
                          <tr>
                            <td>Hệ điều hành</td>
                            <td>{data[index].heDieuHanh}</td>
                          </tr>
                          <tr>
                            <td>Camera trước</td>
                            <td>{data[index].cameraTruoc}</td>
                          </tr>
                          <tr>
                            <td>Camera sau</td>
                            <td>{data[index].cameraSau}</td>
                          </tr>
                          <tr>
                            <td>RAM</td>
                            <td>{data[index].ram}</td>
                          </tr>
                          <tr>
                            <td>ROM</td>
                            <td>{data[index].rom}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
